#!/bin/sh

Kexts=("RealtekRTL8100.kext" "ALXEthernet.kext" "AtherosE2200Ethernet.kext" "GenericUSBXHCI.kext" "VoodooPS2Controller.kext")
for kext in ${Kexts[@]}; 
do
  if [ -e "/Volumes/EFI/EFI/CLOVER/kexts/10.9/$kext" ]
  then
    rm -rf "/Volumes/EFI/EFI/CLOVER/kexts/10.9/$kext"
  fi

  if [ -e "/Volumes/EFI/EFI/CLOVER/kexts/10.10/$kext" ]
  then
    rm -rf "/Volumes/EFI/EFI/CLOVER/kexts/10.10/$kext"
  fi
done