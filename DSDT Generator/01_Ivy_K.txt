#Maintained by: Pokenguyen for: ASUS IvyBridge K Series

# _PLD is supposed to return a variable length Package of Buffers
into_all all code_regex (Name\s*\(_PLD,\s*)Buffer(\s\([^\)].*\)[^\)]*) replaceall_matched
begin
%1Package() { Buffer%2 }
end;

# _PLD return case, courtesy stargazer418
into_all all code_regex (Return\s)\(Buffer(\s\(0x10\)[^\)]*) replaceall_matched
begin
%1(Package() { Buffer%2}
end;

into method label WMMX code_regex Return\s\(\\_SB.PCI0.GFX0._DSM\)[^}]+\} replaceall_matched begin } end;

into method label WMMX code_regex Return\s\(\\_SB.PCI0.GFX0._DSM\s\([^\}]+\} replaceall_matched begin } end;

into method label _ROM code_regex Return\s\(Zero\)  replace_matched begin Return (Buffer(){}) end;

# remove all OEM _DSM methods
into_all method label _DSM remove_entry;

#    Replace all instances of AC with ADP1
into_all all code_regex \.AC0, replaceall_matched begin .ADP1, end;
into_all all code_regex \(AC0, replaceall_matched begin (ADP1, end;
into all label AC0 set_label begin ADP1 end;


#   Power fix - uses AppleACPIBatteryManager.kext  instead VoodooBattery
into device label ADP1 code_regex Name\s+\(_PRW,[\s\S]*\}\) remove_matched;
into device label ADP1 insert
begin
Name (_PRW, Package() { 0x18, 0x03 })
end;

#   Inject Audio info
into method label _DSM parent_label HDEF remove_entry;
into device label HDEF insert
begin
Method (_DSM, 4, NotSerialized)\n
{\n
    If (LEqual (Arg2, Zero)) { Return (Buffer() { 0x03 } ) }\n
    Return (Package()\n
    {\n
        "hda-gfx", Buffer() { "onboard-1" },\n
        "layout-id", Buffer() { 0x1C, 0x00, 0x00, 0x00 },\n
        "PinConfigurations", Buffer() { },\n
        //"MaximumBootBeepVolume", 77,\n
    })\n
}\n
end;


#     IRQ fix
into device name_hid PNP0000 code_regex IRQNoFlags\s\(\)\n\s+\{(\d+)\} remove_matched;
into device name_hid PNP0100 code_regex IRQNoFlags\s\(\)\n\s+\{(\d+)\} remove_matched;
into device name_hid PNP0B00 code_regex IRQNoFlags\s\(\)\n\s+\{(\d+)\} remove_matched;
into device name_hid PNP0103 code_regex IRQNoFlags\s\(\)\n\s+\{.*\} removeall_matched;
into device name_hid PNP0103 code_regex Name\s\(([^,]+),\sResourceTemplate\s\(\)\n\s+\{((?:.|\n)*)\}\) replace_matched
begin
Name (%1, ResourceTemplate()\n
{\n
    IRQNoFlags() { 0, 8, 11, 15 }\n
%2
})\n
end;


#    Rename all instances of GFX0 to IGPU
into_all all code_regex GFX0 replaceall_matched begin IGPU end;
into_all all label GFX0 set_label begin IGPU end;


#   Brightness control fix
into device label PNLF remove_entry;
into scope label \_SB insert
begin
Device (PNLF)\n
{\n
	Name (_HID, EisaId ("APP0002"))\n
	Name (_CID, "backlight")\n
	Name (_UID, 10)\n
	Name (_STA, 0x0B)\n
}\n
end;


#   RTC fix
into device name_hid PNP0B00 code_regex (IO\s\((?:\s*[^,]+,\s*(?:\/\/\s.*)?\s*\n)+\s*)(\dx\d+)(,\s*(?:\/\/\s.*)?\s*\n\s*\)) replace_matched begin %10x02%3 end;


#  Shutdown fix
into method label _PTS code_regex_not If\s\(LEqual\s\(Arg0,\s0x05\)\) code_regex ^((?:.|\n)*)$ replace_matched
begin
    If (LEqual (Arg0, 0x05)) {}\n
    Else\n
    {\n
%1
    }\n
end;


#   SMBUS fix
into device label BUS0 parent_adr 0x001F0003 remove_entry;
into device name_adr 0x001F0003 insert
begin
Device (BUS0)\n
{\n
    Name (_CID, "smbus")\n
    Name (_ADR, Zero)\n
    Device (DVL0)\n
    {\n
        Name (_ADR, 0x57)\n
        Name (_CID, "diagsvault")\n
        Method (_DSM, 4, NotSerialized)\n
        {\n
            If (LEqual (Arg2, Zero)) { Return (Buffer() { 0x03 } ) }\n
            Return (Package() { "address", 0x57 })\n
        }\n
    }\n
}\n
end;

#    USB sleep fix injects Macbook 8,1 device IDs
into method label _DSM parent_adr 0x001D0001 remove_entry;
into device name_adr 0x001D0001 insert
begin
Method (_DSM, 4, NotSerialized)\n
{\n
    If (LEqual (Arg2, Zero)) { Return (Buffer() { 0x03 } ) }\n
    Return (Package()\n
    {\n
        "device-id", Buffer() { 0x27, 0x1c, 0x00, 0x00 },\n
        "name", Buffer() { "pci8086,1c27" },\n
    })\n
}\n
end;

into method label _DSM parent_adr 0x001D0002 remove_entry;
into device name_adr 0x001D0002 insert
begin
Method (_DSM, 4, NotSerialized)\n
{\n
    If (LEqual (Arg2, Zero)) { Return (Buffer() { 0x03 } ) }\n
    Return (Package()\n
    {\n
        "device-id", Buffer() { 0x2c, 0x1c, 0x00, 0x00 },\n
        "name", Buffer() { "pci8086,1c2c" },\n
    })\n
}\n
end;

into method label _DSM parent_adr 0x001D0003 remove_entry;
into device name_adr 0x001D0003 insert
begin
Method (_DSM, 4, NotSerialized)\n
{\n
    If (LEqual (Arg2, Zero)) { Return (Buffer() { 0x03 } ) }\n
    Return (Package()\n
    {\n
        "device-id", Buffer() { 0x27, 0x1c, 0x00, 0x00 },\n
        "name", Buffer() { "pci8086,1c27" },\n
    })\n
}\n
end;

into method label _DSM parent_adr 0x001D0004 remove_entry;
into device name_adr 0x001D0004 insert
begin
Method (_DSM, 4, NotSerialized)\n
{\n
    If (LEqual (Arg2, Zero)) { Return (Buffer() { 0x03 } ) }\n
    Return (Package()\n
    {\n
        "device-id", Buffer() { 0x2c, 0x1c, 0x00, 0x00 },\n
        "name", Buffer() { "pci8086,1c2c" },\n
    })\n
}\n
end;

into method label _DSM parent_adr 0x001A0001 remove_entry;
into device name_adr 0x001A0001 insert
begin
Method (_DSM, 4, NotSerialized)\n
{\n
    If (LEqual (Arg2, Zero)) { Return (Buffer() { 0x03 } ) }\n
    Return(Package()\n
    {\n
        "device-id", Buffer() { 0x27, 0x1c, 0x00, 0x00 },\n
        "name", Buffer() { "pci8086,1c27" },\n
    })\n
}\n
end;

into method label _DSM parent_adr 0x001A0002 remove_entry;
into device name_adr 0x001A0002 insert
begin
Method (_DSM, 4, NotSerialized)\n
{\n
    If (LEqual (Arg2, Zero)) { Return (Buffer() { 0x03 } ) }\n
    Return (Package()\n
    {\n
        "device-id", Buffer() { 0x2c, 0x1c, 0x00, 0x00 },\n
        "name", Buffer() { "pci8086,1c2c" },\n
    })\n
}\n
end;

into method label _DSM parent_adr 0x001A0003 remove_entry;
into device name_adr 0x001A0003 insert
begin
Method (_DSM, 4, NotSerialized)\n
{\n
    If (LEqual (Arg2, Zero)) { Return (Buffer() { 0x03 } ) }\n
    Return (Package()\n
    {\n
        "device-id", Buffer() { 0x27, 0x1c, 0x00, 0x00 },\n
        "name", Buffer() { "pci8086,1c27" },\n
    })\n
}\n
end;


#     HPET fix to avoid AppleIntelCPUPowerManagement panic
into method label _STA parent_hid PNP0103 remove_entry;
into device name_hid PNP0103 code_regex Name\s\(_STA\,\s+0x0F\) remove_matched;
into device name_hid PNP0103 insert
begin
Name (_STA, 0x0F)\n
end;

into method label _CRS parent_hid PNP0103 remove_entry;
into device name_hid PNP0103 insert
begin
Method (_CRS, 0, NotSerialized) { Return (BUF0) }\n
end;


#     Fix _WAK method to deal with bug in AppleACPIPlatform v1.8 (in 10.8.5+)
into method label _WAK code_regex \s*(.?WAK\s+\([^)]*\)) replace_matched
begin
If (LOr(LLess(Arg0,1),LGreater(Arg0,5))) { Store(3,Arg0) }\n
%1
end;

#     Add missing IMEI device
into device label IMEI parent_label PCI0 remove_entry;
into device label PCI0 insert
begin
Device (IMEI)\n
{\n
    Name (_ADR, 0x00160000)\n
}\n
end;

# Add the missing MCHC device
into device label MCHC parent_label PCI0 remove_entry;
into device label PCI0 insert begin
Device (MCHC)\n
{\n
	Name (_ADR, Zero)\n
}
end;


# patch OS checks
into_all all code_regex If\s\(_OSI\s\(\"Windows\s2006\"\)\) replaceall_matched
begin If (LOr (_OSI ("Darwin"), _OSI ("Windows 2006"))) end;

into_all all code_regex If\s\(\\_OSI\s\(\"Windows\s2006\"\)\) replaceall_matched
begin If (LOr (_OSI ("Darwin"), _OSI ("Windows 2006"))) end;

# Disable BAT1 device
into method label _STA parent_label BAT1 replace_content begin Return (Zero) end;

# Fix method PPNT, called from AC adapter device (AC/ADP1) (ProBooks)
into method label PPNT parent_label EC0 replace_content begin // nothing end;

# Fix method PNOT, called from AC adapter device (AC/ADP1) (EliteBooks?)
into method label PNOT replace_content begin // nothing end;

# These are not really battery related, but similar problems...

# Fix method PCNT, called from HWAK which is called from _WAK
into method label PCNT replace_content begin // nothing end;


#Maintained by: RehabMan for: HP Probook 4x30s/4x40s
# 02b_EHCI_4x40s.txt

# USB property injection for 4x40s (HM76)

#   Insert Apple USB properties into EHC1
into method label _DSM parent_adr 0x001D0000 remove_entry;
into device name_adr 0x001D0000 insert
begin
Method (_DSM, 4, NotSerialized)\n
{\n
    If (LEqual (Arg2, Zero)) { Return (Buffer() { 0x03 } ) }\n
    Return (Package()\n
    {\n
        "AAPL,clock-id", Buffer() { 0x01 },\n
        "subsystem-id", Buffer() { 0x70, 0x72, 0x00, 0x00 },\n
        "subsystem-vendor-id", Buffer() { 0x86, 0x80, 0x00, 0x00 },\n
        "AAPL,current-available", 2100,\n
        "AAPL,current-extra", 2200,\n
        "AAPL,current-extra-in-sleep", 1600,\n
        "AAPL,device-internal", 0x02,\n
        "AAPL,max-port-current-in-sleep", 2100,\n
    })\n
}\n
end;

# Insert Apple USB properties into EHC2
into method label _DSM parent_adr 0x001A0000 remove_entry;
into device name_adr 0x001A0000 insert
begin
Method (_DSM, 4, NotSerialized)\n
{\n
    If (LEqual (Arg2, Zero)) { Return (Buffer() { 0x03 } ) }\n
    Return (Package()\n
    {\n
        "AAPL,clock-id", Buffer() { 0x01 },\n
        "subsystem-id", Buffer() { 0x70, 0x72, 0x00, 0x00 },\n
        "subsystem-vendor-id", Buffer() { 0x86, 0x80, 0x00, 0x00 },\n
        "AAPL,current-available", 2100,\n
        "AAPL,current-extra", 2200,\n
        "AAPL,current-extra-in-sleep", 1600,\n
        "AAPL,device-internal", 0x02,\n
        "AAPL,max-port-current-in-sleep", 2100,\n
    })\n
}\n
end;

# Insert Apple USB properties into USB 3.0 XHC
into method label _DSM parent_adr 0x00140000 remove_entry;
into device name_adr 0x00140000 insert
begin
Method (_DSM, 4, NotSerialized)\n
{\n
    If (LEqual (Arg2, Zero)) { Return (Buffer() { 0x03 } ) }\n
    Return (Package()\n
    {\n
        "subsystem-id", Buffer() { 0x70, 0x72, 0x00, 0x00 },\n
        "subsystem-vendor-id", Buffer() { 0x86, 0x80, 0x00, 0x00 },\n
        "AAPL,current-available", 2100,\n
        "AAPL,current-extra", 2200,\n
        "AAPL,current-extra-in-sleep", 1600,\n
        "AAPL,device-internal", 0x02,\n
        "AAPL,max-port-current-in-sleep", 2100,\n
    })\n
}\n
end;

into method label _INI parent_label \_SB.PCI0.PEG0.PEGP insert
begin
//added to turn nvidia/radeon off\n
//External(\_SB.PCI0.PEG0.PEGP._OFF, MethodObj)\n
_OFF()\n
end;

into method label _INI parent_label _SB.PCI0.PEG0.PEGP insert
begin
//added to turn nvidia/radeon off\n
//External(\_SB.PCI0.PEG0.PEGP._OFF, MethodObj)\n
_OFF()\n
end;